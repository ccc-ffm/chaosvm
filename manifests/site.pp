node default {
}

node 'mtbf.ccc-ffm.de' {
  include common
  include infrateam
  include eventcalendar
  include hqsatellite
}

node 'wtf.ccc-ffm.de' {
  include common
  include infrateam
  include inventorydb
}
