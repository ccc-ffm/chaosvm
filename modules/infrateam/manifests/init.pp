class infrateam {
  
  include params

  group { 'adm':
    ensure => present,
    gid => 4,
    system => true,
  }

  group { 'sudo':
    ensure => present,
    gid => 27,
    system => true,
  }
  
  define assigngroups {
    Common::Accountsetup::Account <| title == $name |> {
      groups +> [ 'sudo', 'adm' ],
    }
  }

  assigngroups{$infrateam::params::admins:}

}