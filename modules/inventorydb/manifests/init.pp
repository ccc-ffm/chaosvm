class inventorydb {

  include params
  
  group { 'invdbadm':
    ensure => present,
    gid => 20001,
    system => false,    
  }

  group { 'invdb':
    ensure => present,
    gid => 30001,
    system => false,    
  }

  define assigngroups {
    Common::Accountsetup::Account <| title == $name |> {
      groups +> [ 'invdbadm', 'invdb' ],
    }
  }

  file { '/srv/invdb':
    ensure => directory,
    mode => '0775',
    owner => 'root',
    group => 'invdb',
    require => Group['invdb'],
  }


  file { '/etc/sudoers.d/group-invdb':
    ensure => present,
    mode => '0440',
    owner => 'root',
    group => 'root',
    content => "%invdbadm ALL=(ALL) NOPASSWD: ALL\n",
    require => [ Group['invdbadm'], Package['sudo'], ],
  }

  assigngroups{$inventorydb::params::invdbmgmr:}

}