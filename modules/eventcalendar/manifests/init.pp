class eventcalendar {

  include params
  
  group { 'caladm':
    ensure => present,
    gid => 20000,
    system => false,    
  }

  group { 'cal':
    ensure => present,
    gid => 30000,
    system => false,    
  }

  file { '/srv/cal':
    ensure => directory,
    mode => '0775',
    owner => 'root',
    group => 'cal',
    require => Group['cal'],
  }


  file { '/etc/sudoers.d/group-caladm':
    ensure => present,
    mode => '0440',
    owner => 'root',
    group => 'root',
    content => "%caladm ALL=(ALL) NOPASSWD: ALL\n",
    require => [ Group['caladm'], Package['sudo'], ],
  }

  define assigngroups {
    Common::Accountsetup::Account <| title == $name |> {
      groups +> [ 'caladm', 'cal' ],
    }
  }
  
  assigngroups{$eventcalendar::params::calmgmr:}

}