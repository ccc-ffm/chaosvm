class hqsatellite {

  include params
  
  group { 'hqsadm':
    ensure => present,
    gid => 20002,
    system => false,    
  }

  group { 'hqs':
    ensure => present,
    gid => 30002,
    system => false,    
  }

  file { '/srv/hqsatellite':
    ensure => directory,
    mode => '0775',
    owner => 'root',
    group => 'hqs',
    require => Group['hqs'],
  }


  file { '/etc/sudoers.d/group-hqsadm':
    ensure => present,
    mode => '0440',
    owner => 'root',
    group => 'root',
    content => "%hqsadm ALL=(ALL) NOPASSWD: ALL\n",
    require => [ Group['hqsadm'], Package['sudo'], ],
  }

  define assigngroups {
    Common::Accountsetup::Account <| title == $name |> {
      groups +> [ 'hqsadm', 'hqs' ],
    }
  }
  
  assigngroups{$hqsatellite::params::calmgmr:}

}