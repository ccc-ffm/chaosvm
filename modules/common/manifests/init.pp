class common {
  
  file { '/usr/local/sbin/git2puppet':
    ensure => present,
    content => "#!/bin/bash\ncd /etc/puppet && git pull && puppet apply /etc/puppet/manifests/site.pp\n",
    mode => '0755',
  }
  
  define set_mountpoint_option($mount, $option) {
    augeas{ "fstab-$mount-$option":
      context => "/files/etc/fstab/*[file = '$mount'][count(opt[. = '$option']) = 0]",
      changes => [
        "ins opt after opt[last()]",
        "set opt[last()] $option",
        ],
      onlyif => "match /files/etc/fstab/*[file = '$mount'][count(opt[. = '$option']) = 0] size > 0",
    }
  }
  
  set_mountpoint_option { '/usr-nodev':
    mount => '/usr',
    option => 'nodev',
  }

  set_mountpoint_option { '/var-nosuid':
    mount => '/var',
    option => 'nosuid',
  }
  
  # packages without configuration
  $packages = [ 'git-core',
                'puppet',
                'screen',
                'rsync',
                'vim',
                'lsof',
                'pv',
                'tcpdump',
               ]
  
  package { $packages:
    ensure => installed,
  }
  
  package { 'sudo':
    ensure => installed,
  }
  
  augeas { 'enable NOPASSWD for %sudo':
    context => '/files/etc/sudoers/spec[2]/host_group/command/',
    changes => [ 'set tag NOPASSWD' ],
    require => Package['sudo'],
  }
  
  # Zeitabgleich
  package { 'chrony':
    ensure => installed,
  }
  
  service { 'chrony':
    ensure => running,
    enable => true,
    hasstatus  => false,
    hasrestart => true,
    require => Package['chrony'],
  }

  # E-Mail via Smart Host
  package { 'nullmailer':
    ensure => installed,
  }
  
  file { '/etc/nullmailer/adminaddr':
    ensure => present,
    content => "infra@ccc-ffm.de\n",
    require => Package['nullmailer'],
    notify => Service['nullmailer'],
  }

  file { '/etc/nullmailer/defaultdomain':
    ensure => present,
    content => "ccc-ffm.de\n",
    require => Package['nullmailer'],
    notify => Service['nullmailer'],
  }

  file { '/etc/nullmailer/remotes':
    ensure => present,
    content => "mx01.ccc-ffm.de\n",
    require => Package['nullmailer'],
    notify => Service['nullmailer'],
  }

  service { 'nullmailer':
    ensure => running,
    enable => true,
    hasstatus  => true,
    hasrestart => true,
  }

  # Software Updates
  package { 'apt-dater':
    ensure => installed,
  }
  
  file { '/etc/sudoers.d/apt-dater-host':
    ensure => present,
    mode => '0440',
    owner => 'root',
    group => 'root',
    content => "# apt-dater may run aptitude as root\napt-dater ALL=NOPASSWD: /usr/bin/apt-get\n",
    require => [ Package['nullmailer'], Package['sudo'] ],
  }
  
  user { 'apt-dater':
    ensure => $ensure,
    comment => $comment,
    gid => 'nogroup',
    membership => inclusive,
    home => '/home/apt-dater',
    managehome => true,
    shell => '/bin/bash',
    system => true,
    require => Package['nullmailer'],
  }
  
  ssh_authorized_key { 'apt-dater noc.cash-zone.de':
    ensure => present,
    user => 'apt-dater',
    type => 'ssh-rsa',
    key => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAp+P0huFZ8h5GjZhtN2ZzM78pc30u2ZVrbmjLoGq9vYXBk2/jCIEWqg+L63EWg2EZcDsbxuKaf4/CYYnB213FYjhlhi8kvt/Gt3GTOxpf1/vEx+VZWpafeTDiTlKzqDHuFMHe+pEMe/OwIuK561ubttUAk6raixgkjxk0WYQX8HWLrO+jyyXstPmqs6lvQ7TYQajC8HmHb5vQWWSNWdcoeybMY+iD7H6e+4oAINs3yVMJN7Lfso7CySN1eYlFfsKExWJ59U1CrUbOgGfz5K6ommtSUqtxl+DOfmXVO40lpQ8iuTZ3YFZPuVunkw5Ce185DfZhKCSQqr+k+gKy2UFnVw==',
  }
  
  ssh_authorized_key { 'backuppc systemgemisch':
    ensure => present,
    user => 'root',
    type => 'ssh-rsa',
    options => [ 'no-port-forwarding',
                 'no-X11-forwarding',
                 'no-agent-forwarding',
                 'from="5.231.239.2,::ffff:5.231.239.2"'
               ],
    key => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAygsqWq5lKygdAqO+GZGRB3t2P5FeQO8a3FZHKCOTpiI8CTycDZpiQnc6a/IsTHP4YvqhX9Swofu9jFDlVQXAExvuKmOlg5HZWAlCPxwMRarwN4QJvOowx+zoK0GVh3X/uNzMwKaNtk4GWek64KaLNx3TmO/UNBJhz9MxsUYvzCTTwi7361Nytko3v9BoJOGYLBYwbnseHsu3oLOYThL+KK8gNFuEMuCTIYK5wipbMbtIcCAIlnNrZTFJz0/6fbEj2A59oCeV98JWWYjmoIDqGTR5RflkuTS1LQnd/NlD+vCJdzf81hIBCbYXsy7+wc1bWj9SJuc2Lt91Qaf1DbT9Lw==',
  }
  
  include common::accountsetup
  include common::users
  
}