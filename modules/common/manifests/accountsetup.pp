class common::accountsetup {

  # required to delete primary usergroups
  augeas { 'allow_not_existent_group':
    context => '/files/etc/login.defs',
    changes => [ 'set USERGROUPS_ENAB no' ],
  }

  define account (
    $comment,
    $groups,
    $uid,
    $sshtype,
    $sshkey,
    $ensure,
    $membership = 'inclusive',
    $shell = '/bin/bash',
  ) {
    
    user { $name:
      ensure => $ensure,
      comment => $comment,
      uid => $uid,
      gid => $name,
      groups => $groups,
      membership => $membership,
      home => "/home/$name",
      managehome => true,
      shell => $shell,
      require => Augeas['allow_not_existent_group'],
    }
    
    group { $name:
      ensure => $ensure,
      system => false,
      gid => $uid,
    }
  
    ssh_authorized_key { $name:
      ensure   => $ensure,
      user     => $name,
      type     => $sshtype,
      key      => $sshkey,
    }
    
  }
  
  # make sure all absent users are realized!
  Account <| ensure == absent |> {
    groups => 'nogroup',
  }

}