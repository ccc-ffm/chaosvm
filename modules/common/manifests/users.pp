class common::users {

  @common::accountsetup::account { 'mschuster':
    ensure => present,
    comment => 'Markus Schuster',
    uid => 1000,
    sshtype => 'ssh-dss',
    sshkey => 'AAAAB3NzaC1kc3MAAAEBANLHnxKhSkYGfYbL6C801cqoXH4lIfV40ebfiiVEqZkYyQotsi0UHYtpfGXH39yydD7J9mWXu+zypKX4II1CZVk3ohemQVe9NZuvRzVlya2oO+FKLUw86cY65XPak0fIY9pGJuTxMLz7PczmBF53jgpq4e3q6nAaf98hzDfWqp2l6Gw7miLNgsQ2e81znH68IchRPSngjVncTxsUL1ok+Ve/U4WKSGYdB1ATJhnZZ76ug41yxGwKYb5ZwjRFC6Y54r7vg3SOhEu3YQ65KRlbzHoGhAckCFqoyA+Z0S+2e71V/JSbnXf0AdFsRJUfWWBi5HxgCX83XNLRqqrHQ19wZx8AAAAVANa0KMWZaIhiHCcPd5aUXqCXn1onAAABAHzBpd5SmYFTsyFBgiQRBn31KCptzehalZ6whaOY8SyZR4Brqyp3ryaqkww3NdX2D8fQfT2X+ZPKfyFXHl4SyOq5XvvE9qcB/kC2e8JsntJ3yKbWNnMlXPiMYkuTYrrpwilI95Sa2ndH5AnntWy5iBqgny/ElI5qABLfG5ZBkOUEuTVJtYN0qqW9Xupwe5XZjwQ4LpA/SqsdD7Zx8zxXObdzNyXDtm9HpQZ8InmHF8VWzx+HAV/7iF58emLbctbqMrBfsfbb2abwv0KrE/8f8bds+vvP00cYwcH3/+5tie+gkgIThFbO1CAddhpkUKKiTNIwUYQdIUZIb2cbJzwr57YAAAEAfGg4nPZgfL/PjIWRPGlD+VFCzocdS/9z9DpdrumIGr+tVcEE++jkwrkPPiAQiI340K4nFj+iRd/w1Xqo5noColi0USsecTeeC3mFhIWGqvgbro/V574rISGRPnLmSjXxmiZWhMFpMlmF2eMYKL7oNIXttVBMrAK84V3shvu20omHfFbVYdWP7uMN8WLs7LOZ+I3gI+DzLXXMfqCxW+qfkfHXPk62vq+BmARNgDpsExLYRCqG4olhDyoZZQjT3El7/QZVmy5YlhJzeJdXpEJFnBv9Fh1NmrIXz+soKcSo3Pu6AvMmfFR0EYeDqcs19QMpofemnAnHm9BTH9DA94RSMQ==',
  }
  
  @common::accountsetup::account { 'phofmann':
    ensure => present,
    comment => 'Pascal Hofmann',
    uid => 1001,
    sshtype => 'ssh-rsa',
    sshkey => 'AAAAB3NzaC1yc2EAAAABIwAAAQEA7cWQ6yRH9d4EQv6Om5u6Xa5eXhbNrtEUnQuk408iquJFL22Ok3EvnHAFHycmtbngCR23Cu/TuwkjW9lYq7tO5NLmc+80Kc6WCHZbKX/ST6ShVhm7U49WUtYkx0AC6PJl1MxTWRiNzriRY8zZ+ENTXph27py3mkGgbTzHeiAicP3lhQ/LMYwZzpCJ80vxgKKHvOlVYRA/NXP8YfKA4dQK6AS5ecFK2S0h5J27zFBorO3uLO78hdRdduyF/uzCfEgWud+xXnk8uhe29+twJTS2WF7CnGjuFU3OzHAZEjQK8s3GHFa56QrhTFPdzn9cWnJrRYAB+7lJ7gXEMJBXimoBAw==',
  }

  @common::accountsetup::account { 'ygoebel':
    ensure => present,
    comment => 'Yves Goebel',
    uid => 1002,
    sshtype => 'ssh-rsa',
    sshkey => 'AAAAB3NzaC1yc2EAAAABIwAAAgEA49yBpOBHpaX4DtrORmmLL18PsnFMB7uDWKjtOGfKPyUdhmhayHmpqFpDojOPOtuceCA5I1VsGL8TnIJHmqCr15fmbCcOuW+LLuraGLQLGiaFOB8ueaQgSjivjxMerSA902RFPrWHI2o7sdYkqfFAsGFSzoULgV4l/6E2Fbx7egf0IxgxRzD9K9bTTyEIGNQtcVAIHxrsisyre94j/Xqh4qwkJWrqhoJJv3iUVenscDxHA01nwVdtVo9z3p53SGVb8kvuyTyVLly2NT3hCoyguAz1t8P/yovQ/6Au+gCi9nvECdhXjtBRAyHzAQKI1ugRL2PDqZuC371URalj6RV5epdU/g2QDLjKE27vTjWb2BPDV2fhdh4cARCGlstzTsMPIx0R/+MnHnj1HcSb0qoLTJXDjVavtmG2LQKUqBs7ll4E6r/gQHIJMew2FXJgcwHHRmGAUi/7XJqdkwu5Axye3RwWm47pIbWWsyEDSY7SiYSc70QCrQl8cjQldBsV9umt7ala2WPRAcm1sSJzvOQX5GSz41YbZJ5XGRAnmW9ZeTN/aum1iqxLdkqYot4vy9Zzc/CRAOtd3Yh2376R3FZepR1rNjRSXSliwNuVFrZoqoRcaQdYRynQDZoXirE50noeuqJe4tPDg1voiOOFRzOYUozYnjdFXXv0B2MCcwLkKt8=',
  }

  @common::accountsetup::account { 'acoutandin':
    ensure => present,
    comment => 'Arwed Coutandin',
    uid => 1003,
    sshtype => 'ssh-rsa',
    sshkey => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQCy1WLtWfqorPk0+flp4Yjn9Phm5SK0zWB4pjTb8bNXZE/uHs7+/SUyIiLWh3lb0e7a3wv6CqiULlZYxs1Rx9r2b8AGU9ik8gvab9hteDjti0wC2xA+WLiFteRHfripM+XiNqpFbpb4FjlG6XlwZNJBENYw8WVLZDu+SbTtNAaqdRTSDUn6dKRZ+ZjkKl4mo6OyZ5m1ICnHYJtCJXovyTBpYsQNQvf58DUEGUeNYOS7CKXpuW+E9YwK4hmHJkoIlu2m4r8v/Lhy12ZU4EhSfx0+QqhbXN2hyr3FquAWMwnz/j34W7s5oX1Ls5zi5LK5dw2eUKq1sgLOWScCBqjOCdEQnRzwnfWDVPcEWmbGX4AY8g7cO5FuAH7+Lyh5qXsyAX3PNd+VchYbZcwtWZV7dEoYTa3ux+PrbOIuAsn5lxxi9+lU+UHGbr7RfeQwohsFaydEpLNF5VCVky98bPDZ0PMfIr2EcuoTDbvN7guUHNNuNNOgepHdU3ANi4QvgfI8YPvqpz+lIBHYeBYX2mFakqiTTuJ6d+DYtGoDt62dY/YIuWfioDxlInRWeCmqchmbG4jbt/ZnCiKLfIXUlN9GnEpeVi3nK5AECdFIWAserQxmy3AuR1UTdGb8jUipOoyBqgfjV0NRXE/Pc8Hhhy0DErxpxxPOH5M7Jk6f1tIRVCzVCQ==', # arwed@macbook-pro.fritz.box
    # sshkey => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDM2nf5wbWoSDGAHRx+1KH1Scmlr62guK3eaaQVl1ts9JdGuoKN8jZxQN+QwXTC6hp+KugSHVacJIya0JWCndRwrUckY7a/2lACzj4E+HXb9mTlgfqA7cQiu1mtl+JBnriNtsKxjQU/C0XpEcR8aSLepOTenQc9DNeMsVkfPt2qAd1LJYskqKgTQC7hgtUmZfSrY5AqS0cvcLqhDZtZ6IGko/bMCnv7hcOPmhxJdpQF6HUnby0EbIOZsftAwRDZvcaGH1qvghp9N312h2eedJCQB6H/Xvz37D5oC7aQReTQ+O9N/75E9frTiVnmM1S20u0m6ZDYXdGGMi36yPY7KJQ4uER1TWHSZT8E+FJVKxDj1NOC16BH4lOMSN+AwR0GJ1NAB6f8SEjsHZjqUWtMov93H27IjnIcwTnarszdiYDasl/+pcL4O54eow+uRZejUauYUvitv4++jzqog9RRQSIXc43Kj4E+TBkPSmPEupyMgTRir5xK4TWrC4BrknN16ouyv4yQ1ZPvLCiuPaNJmVNOMCuQZsjOpBwiLKGt2FTKXg8FCV6UhtbAkgCQZhfrcaGcSJcPl9TzqnA+phKtx1+I/De0O+69K2nwJoj45O1qVuLFTiMwJYwvEbPo4QfUOlSxv5cj99USVpXwQFZ4q8Eyz9/WvXOh2KZdFGpDyEj6wQ==', # arwed@macbookair
  }

  @common::accountsetup::account { 'aschiermeier':
    ensure => present,
    comment => 'Andreas Schiermeier',
    uid => 1004,
    sshtype => 'ssh-rsa',
    sshkey => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQCjOdP3+ujw2DNNMBYDkyDlSV4MQ5/fiJwX8d62NvwkxUX0rHbPcoQdnwuHakwRgJs9azE/KIa3PlCw4vd2tBF+WRNcCcA/483FUFAlIuzt8kvopt4X7yxgCL4K5yJsWhn9jTHRaQ0yZEmABLSJ4LO3Fl0m8eAyol4ylLvT/+e+lNskEC4e8CqfEzVTeOUefe62Ick1BbPGu1LyliJIpQ0y/up0deOylbWf7ll5+22wqxF89et7ZS+xKtdzJ/whvFeWlUvSdErYfWwGzYuGx/5HaWklKjXNQ7Y4ulM5OFD2yonnDi+CQzn5wc/VxVIgqDiqZI1hDXdus5GklcBaaXDsYoEImppyL+18aJ5YKX3vAVt5fW6q94yzS9v3sQqUMsKhgoFcdx645txwgfNgqsRNTXG1SgaamH4HEkwkawoSYI/QWcPgWJbwVG9nTkEpRh8/feFv29LMepSQqWGHoXBvz3xphLV5sQgo6U2h4HOuNC3nFa6KdWnllnL7JMEluyKyZSegHytio87RrHkkhsE0NgFvqWGxlTq3016iglAdHsQn5KJUdICY1bOVGnesJjo0AKKUydW/pQJU82h/V+CeBkImzyX+CLXMpJitU0BE2Bq2ZdhpBHlP3DICsPwbTheBcL8AQUaLo9iTiaFk4VnSnj92wLra5YnevP2U3nL3rQ==',
  }

  @common::accountsetup::account { 'mandre':
    ensure => present,
    comment => 'Max Andre',
    uid => 1005,
    sshtype => 'ssh-rsa',
    # sshkey => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDNuRDKJUsn8AALnw18GZinTgbIozx9JsKoIOhQqGkQ0npePebqH6CuKM72uzhmp7K/LvTkwQX9C4pCktFJubKiGu3viohp8YtoaiB8KPjAnvg+DVU9DsyuIj8lzgmSuMWHFKGkdvIojQ1Jy2Dnz6LmWU1K2NaRi4iZgIJwTY++RbzcNij1YanqWJu8Gtj5Sw59ulUQqMN6h4/wozFhEcnP63crzgtGU+vAC63Id6gWoaVYj4B1JS9g1HIU6dr6kuKl6JJ/dygqSUhxIWOEn5qBd0Oa19hxRYQq1yYAkIBz6BSMO1S6etu8ibj1xk3yqY++63cuNv+BsetGm2Fs5HBVVvjYg11pLanRt+nL+cMwhyZlI8DeSwhRiIH3QTqS/p8KpR1Bvsuj+aIqAMQzWpjL3ROMUxlIu7MFmwXR1L3+B84DnVHf15QqE1cESeYJVc7qOSbdtv+eAdZIKT2sHk2uYva1g4R93AlZ6LqdD6vZfV3333OOJk8DNYTgzJ1yfA+3VXRvlRuYhFUqXmdcNjOuT7JHbvcD3zmGtcS7o34zqBOpncBAAoDhuObje0qA8QGXRIemzSeeoT6fTIkcHCD9LDkPJUUNL5556b1aHjfgC7ryBb4V/sNofOuG3Azpq+VVc5NG2o0pdNfb4hZVomBe++OkoPuNKBHPysFeicSl4w==',
    sshkey => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQCrWLRbeo1f71Uqm2t9jMfz/dt41d9Jyx6P2vFRUGWgcuqaxbSS/Il/NRwcAulJQrpXiZQ58Ka7eTWw0fcEBvIbVtP6D8pUFN16dd7tgcb9JgKO/15R5mz1bOlCPJkxcHf+I00i+iN/ZwFR/OLqwpmyXWEqbFkm5yPuDiRglBljQCn3FNId77BgZpdCAPHC2E9g3aBaqxZfCv6uGyLU7aHahacrhKVulpXMNgfsWj0yp3vXP4dlvxP6sSicxaqG8IDSEgJ88nTk8QyOaHqakvF79400Emx62A7s63Lc2yO202ZlOYT/oDBJwzFdem/v1vBCTx6tDbsi/NOzI5B3M+e87ax0904DPFNQV2JUU5bgtsR2Uf9smLByS4oAymg2x/mZHqPbYQRwaAQm/SNpsUT5uMZkRGGLJ8oho0km/sGFndzxNe5WLHxvELGLimOXQPXzOXX0izi6O5krLc5DffzWQcHfjGZs6tFyLHaTYWaz/F0X2oxDoiUGnE8iCivjUVUjMSdbl66Sr5SEHp/UXtpSu0F2dbQZoy1C5sKYCHxo4LAAMzHAGETfkU1ajpivpo0qLFYS68LmBVjekMtZ46wBlVZCcgmVEcFx+sjgQxcN4AoS3CUI1OJZupJvwIkt9qbCC4kFC8FYfDCCqmWKKfOXEKua5L0Wk2KfMjbiW3tPgQ==',
  }

  @common::accountsetup::account { 'kwolff':
    ensure => present,
    comment => 'Kris Wolff',
    uid => 1006,
    sshtype => 'ssh-rsa',
    sshkey => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCnvAXTLtbkUOFb19Ax57QNSHD/w1MlrqeXPQK/Pegi/uqfFX500UZ/g5vKxuUkhSsPBlZRPKX+458W5R71MHvFR4rQdZrN0FxLLdkBbtwze7NcVqHgynF5KM+RhpSe27oK90FtDlLc6ls7stPdSRwjkDEon7BUyHYhhq+1kiQ/kWSuZY69iY6K4kduh0ZNBdq4bdUGMu0PeaXSJ3P4txRayR7wJEpXrFexnRwbwqiiGpKTieOPNnG38theJ7rJizytHk/j9S8Wk2Cbe6I/S8IA13lnoRzDR/DVUnqDfEQg5/KAz9Vjui3swgyAvEr9e5m70fxzQsVOuplY+49sfxWR',
  }

}